import java.math.BigInteger;
import java.util.Scanner;

public class Homework1 {
    public static Scanner in = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Task 1: ");
        System.out.println(sum());
        System.out.println("Task1 ended");
        System.out.println("Task 2: ");
        System.out.println(shar());
        System.out.println("Task 2 ended");
        System.out.println("Task 3: ");
        System.out.println(binary());
        System.out.println("Task 3 ended");
        System.out.println("Task 3 Alternative: ");
        System.out.println(binaryAnother());
        System.out.println("Task 3 Alternative ended");
    }
    public static int sum() {
        int a = in.nextInt();
        int b = in.nextInt();
        return Math.abs(a - b);
    }
    public static String shar() {
        int r = in.nextInt();
        double v = (4.0/3.0) * Math.PI * Math.pow(r, 3);
        double s = 4 * Math.PI * Math.pow(r, 2);
        return "Объём: " + v + "\nПлощадь: " + s;
    }
    public static String binary() {
        int k = in.nextInt();
        return Integer.toBinaryString(k);
    }
    public static String binaryAnother() {
        BigInteger a = new BigInteger(in.next());
        return a.toString(2);
    }
}
