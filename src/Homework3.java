import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Homework3 {
    public static Scanner in = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Task 1");
        System.out.println(students());
        System.out.println("Task 1 ended");
        System.out.println("Task 2");
        System.out.println(maxOfAbs());
        System.out.println("Task 2 ended");
        System.out.println("Task 3");
        System.out.println(amountOfLocalMax());
        System.out.println("Task 3 ended");
        System.out.println("Task 4");

    }
    public static String students() {
        int n = in.nextInt();
        int[] a = new int[n];
        int middle = 0;
        for (int i = 0; i < n; i++) {
            a[i] = in.nextInt();
            middle += a[i];
        }
        middle = middle / n;
        int count = 0;
        for (int i = 0; i < n; i++) {
            if (a[i] > middle) {
                count++;
            }
        }
        return "Middle height: " + middle + "\nAmount of Student who are higher then Middle: " + count;
    }
    public static int maxOfAbs() {
        int n = in.nextInt();
        ArrayList<Integer> a = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            a.add(Math.abs(in.nextInt()));
        }
        int index = 0;
        int maxValue = Collections.max(a);
        for (int i = 0; i < a.size(); i++) {
            if (maxValue == a.get(i)) {
                index = i;
            }
        }
        return index;
    }
    public static int amountOfLocalMax() {
        int n = in.nextInt();
        int[] a = new int[n];
        int cnt = 0;
        for (int i = 0; i < n; i++) {
            a[i] = in.nextInt();
        }
        for (int i = 1; i < a.length - 1; i++) {
            if (a[i] > a[i - 1] && a[i] > a[i + 1]) {
                cnt++;
            }
        }
        return cnt;
    }
    public static boolean sequence() {
        int n = in.nextInt();
        int[] a = new int[n];
        boolean flag = true;
        for (int i = 0; i < n; i++) {
            a[i] = in.nextInt();
        }
        int x = in.nextInt();
        for (int i = 1; i < n; i++) {
            if (i <= x) {
                if (a[i] < a[i - 1]) {
                    flag = false;
                }
            } else {
                if (a[i] > a[i - 1]) {
                    flag = false;
                }
            }

        }
        return flag;
    }
}
