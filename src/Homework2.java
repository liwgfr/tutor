import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class Homework2 {
    public static Scanner in = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Task 1: ");
        System.out.println(difference());
        System.out.println("Task 1 ended");
        System.out.println("Task 2");
        System.out.println(maxAndPrevMax());
        System.out.println("Task 2 ended");
        System.out.println("Task 3: ");
        System.out.println(couple());
        System.out.println("Task 3 ended");
        System.out.println("Task 4: ");
        System.out.println(sumOfDigits());
        System.out.println("Task 4 ended");
        System.out.println("Task 5");
        System.out.println(localMax());
        System.out.println("Task 5 ended");

    }
    public static int difference() {
        int q = in.nextInt();
        int odd = 0;
        int even = 0;
        for (int i = 0; i < q; i++) {
            int z = in.nextInt();
            if (z % 2 == 0) {
                even += z;
            } else {
                odd += z;
            }
        }
        if (odd == 0) {
            return 0;
        }
        if (even == 0) {
            return 0;
        }
        return even - odd;
    }
    public static String maxAndPrevMax() {
        int q = in.nextInt();
        ArrayList<Integer> a = new ArrayList<>();
        for (int i = 0; i < q; i++) {
            a.add(in.nextInt());
        }
        a.sort(Comparator.reverseOrder());
        return "MAX value: " + a.get(0) + "\nPrevious MAX value: " + a.get(1);
    }
    public static int couple() {
        int q = in.nextInt();
        int min = Integer.MAX_VALUE;
        int[] a = new int[q];
        for (int i = 0; i < q; i++) {
            a[i] = in.nextInt();
        }
        for (int i = 1; i < a.length; i++) {
            int abs = Math.abs(a[i - 1] - a[i]);
            if (abs < min) {
                min = abs;
            }
        }
        return min;
    }
    public static int sumOfDigits() {
        int q = in.nextInt();
        int max = Integer.MIN_VALUE;
        int value = 0;
        for (int i = 0; i < q; i++) {
            String v = in.next();
            for (int j = 0; j < v.length(); j++) {
                value += v.charAt(j) - '0';
            }
            if (max < value) {
                value = max;
            }
        }
        return value;
    }
    public static String localMax() {
        int n = in.nextInt();
        int[] a = new int[n];
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < n; i++) {
            a[i] = in.nextInt();
        }
        for (int i = 1; i < a.length - 1; i++) {
            if (a[i] > a[i - 1] && a[i] > a[i + 1]) {
                s.append(a[i]).append(" ");
            }
        }
        return s.toString();
    }
}
